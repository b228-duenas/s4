console.log("Hello World");

class Student {
  // constructor keyword allows us to pass arguments when we instantiate objects from classes
  constructor(name, email, grades) {
    this.name = name;
    this.email = email;

    this.gradeAve = undefined;

    this.passed = undefined;
    this.passedWithHonors = undefined;

    // ACTIVITY 1 Item 1
    //check first if the array has 4 elements
    if (grades.length === 4) {
      if (grades.every((grade) => grade >= 0 && grade <= 100)) {
        this.grades = grades;
      } else {
        this.grades = undefined;
      }
    } else {
      this.grades = undefined;
    }
  }
  login() {
    console.log(`${this.email} has logged in`);
    return this;
  }
  logout() {
    console.log(`${this.email} has logged out`);
    return this;
  }
  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
    return this;
  }
  computeAve() {
    let sum = 0;
    this.grades.forEach((grade) => (sum = sum + grade));
    // update the gradeAve property
    this.gradeAve = sum / 4;
    // returns the object
    return this;
  }
  // ACTIVITY 1 Item 2
  willPass() {
    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
    return this;
  }
  // ACTIVITY 1 Item 2
  willPassWithHonors() {
    if (this.passed) {
      if (this.gradeAve >= 90) {
        this.passedWithHonors = true;
      } else {
        this.passedWithHonors = false;
      }
    } else {
      this.passedWithHonors = false;
    }
    return this;
  }
}

// Section Class to store multiple Student classes
class Section {
  // every Section object will be instantiated with an empty array "students"
  constructor(name) {
    this.name = name;
    this.students = [];
    this.honorStudents = undefined;
    this.honorsPercentage = undefined;
  }

  // this will take in the same arguments needed to instantiate a Student object
  addStudent(name, email, grades) {
    // a Student object will be instantiated before being pushed to the students array property
    this.students.push(new Student(name, email, grades));
    // return the Section object, allowing us to chain this method
    return this;
  }
  // every student object will have the properties and methods defined in the Student class
  countHonorStudents() {
    let count = 0;
    this.students.forEach((student) => {
      // passedWithHonors property will only be updated when willPassWithHonors() is invoked
      // willPassWithHonors() depends on the execution of willPass()
      // willPass() relies on the gradeAve to be computed by the computeAve()
      if (
        student.computeAve().willPass().willPassWithHonors().passedWithHonors
      ) {
        count++;
      }
    });
    this.honorStudents = count;
    return this;
  }
  countHonorsPercentage() {
    this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
    return this;
  }
}

class Grade {
  constructor(level) {
    this.level = level;
    this.sections = [];
    this.totalStudents = 0;
    this.totalHonorStudents = 0;
    this.batchAveGrade = undefined;
    this.batchMinGrade = undefined;
    this.batchMaxGrade = undefined;
  }

  addSection(name) {
    this.sections.push(new Section(name));
    return this;
  }
  countStudents() {
    let total = 0;
    this.sections.forEach(
      (section) => (total = section.students.length + total)
    );
    this.totalStudents = total;
    return this;
  }
  countHonorStudents() {
    let total = 0;
    this.sections.forEach((section) =>
      section.students.forEach((student) => {
        if (
          student.computeAve().willPass().willPassWithHonors().passedWithHonors
        ) {
          total++;
        }
      })
    );
    this.totalHonorStudents = total;
    return this;
  }
  computeBatchAve() {
    let sum = 0;
    this.sections.forEach((section) =>
      section.students.forEach((student) => {
        sum = student.computeAve().gradeAve + sum;
      })
    );
    this.batchAveGrade = sum / this.countStudents().totalStudents;
    return this;
  }
  getBatchMinGrade() {
    let arr = [];
    this.sections.forEach((section) =>
      section.students.forEach((student) =>
        student.grades.forEach((grade) => {
          arr.push(grade);
        })
      )
    );
    this.batchMinGrade = Math.min(...arr);
    return this;
  }
  getBatchMaxGrade() {
    let arr = [];
    this.sections.forEach((section) =>
      section.students.forEach((student) =>
        student.grades.forEach((grade) => {
          arr.push(grade);
        })
      )
    );
    this.batchMinGrade = Math.max(...arr);
    return this;
  }
}

const grade1 = new Grade(1);

grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");

const section1A = grade1.sections.find(
  (section) => section.name === "section1A"
);
const section1B = grade1.sections.find(
  (section) => section.name === "section1B"
);
const section1C = grade1.sections.find(
  (section) => section.name === "section1C"
);
const section1D = grade1.sections.find(
  (section) => section.name === "section1D"
);

section1A.addStudent("John", "john@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Joe", "joe@mail.com", [78, 82, 79, 85]);
section1A.addStudent("Jane", "jane@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Jessie", "jessie@mail.com", [91, 89, 92, 93]);

section1B.addStudent("Jeremy", "jeremy@mail.com", [85, 82, 83, 89]);
section1B.addStudent("Johnny", "johnny@mail.com", [82, 86, 77, 88]);
section1B.addStudent("Jerome", "jerome@mail.com", [89, 85, 92, 91]);
section1B.addStudent("Janine", "janine@mail.com", [90, 87, 94, 91]);

section1C.addStudent("Faith", "faith@mail.com", [87, 85, 88, 91]);
section1C.addStudent("Hope", "hope@mail.com", [85, 87, 84, 89]);
section1C.addStudent("Love", "love@mail.com", [91, 87, 90, 88]);
section1C.addStudent("Joy", "joy@mail.com", [92, 86, 90, 89]);

section1D.addStudent("Eddie", "eddie@mail.com", [85, 87, 86, 92]);
section1D.addStudent("Ellen", "ellen@mail.com", [88, 84, 86, 90]);
section1D.addStudent("Edgar", "edgar@mail.com", [90, 89, 92, 86]);
section1D.addStudent("Eileen", "eileen@mail.com", [90, 88, 93, 84]);
